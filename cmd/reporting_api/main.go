package main

import (
	configreader "firefly/framework/config_reader"
	mongodbpersistence "firefly/framework/persistence/mongodb_persistence"
	"firefly/pkg/api"
	commandhandlers "firefly/pkg/application/command_handlers"
	httpport "firefly/pkg/infrastructure/http_port"
	"firefly/pkg/infrastructure/repositories/mongodbimpl"

	"github.com/adzeitor/mediatr"
	"github.com/gin-gonic/gin"
)

func main() {
	mr := mediatr.New()
	register(mr)
	httpPort := httpport.NewHttpPort()
	r := gin.Default()
	g := r.Group("api/v1")
	reportingController := api.NewReportingController(mr, httpPort)
	reportingGroup := g.Group("reporting")
	reportingGroup.GET("/:geometryId/count", reportingController.GetCount)
	r.Run(":8080")
}

func register(mr mediatr.Mediator) {
	path := "configs"
	fileType := "yaml"
	name := "config"
	configReader, err := configreader.NewConfigReader(name, path, fileType)
	if err != nil {
		panic(err)
	}
	mongoConnection, err := mongodbpersistence.NewMongoPersistence(configReader)
	if err != nil {
		panic(err)
	}
	reportingRepository := mongodbimpl.NewReportingRepository(mongoConnection)
	mr.Register(commandhandlers.NewGetOrderCountByGeometryCommandHandler(reportingRepository).Handle)
}
