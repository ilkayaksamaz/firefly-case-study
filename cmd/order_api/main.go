package main

import (
	configreader "firefly/framework/config_reader"
	mongodbpersistence "firefly/framework/persistence/mongodb_persistence"
	"firefly/pkg/api"
	commandhandlers "firefly/pkg/application/command_handlers"
	eventhandlers "firefly/pkg/application/event_handlers"
	queryhandlers "firefly/pkg/application/query_handlers"
	eventdispatch "firefly/pkg/infrastructure/event_dispatch"
	httpport "firefly/pkg/infrastructure/http_port"
	"firefly/pkg/infrastructure/repositories/mongodbimpl"

	"github.com/adzeitor/mediatr"
	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	// swagger embed files
	docs "firefly/cmd/order_api/docs"

	swaggerfiles "github.com/swaggo/files"
)

func main() {

	docs.SwaggerInfo.BasePath = "/api/v1"

	mr := mediatr.New()
	register(mr)

	httpPort := httpport.NewHttpPort()
	orderController := api.NewOrderController(mr, httpPort)
	r := gin.Default()
	g := r.Group("api/v1")
	orderGroup := g.Group("order")
	orderGroup.GET("/:orderId", orderController.Get)
	orderGroup.PUT("/:orderId", orderController.Update)
	orderGroup.POST("/", orderController.Create)
	orderGroup.DELETE("/:orderId", orderController.Delete)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	r.Run(":8080")
}

func register(mr mediatr.Mediator) {
	path := "configs"
	fileType := "yaml"
	name := "config"
	configReader, err := configreader.NewConfigReader(name, path, fileType)
	if err != nil {
		panic(err)
	}
	mongoConnection, err := mongodbpersistence.NewMongoPersistence(configReader)
	if err != nil {
		panic(err)
	}
	orderRepository := mongodbimpl.NewOrderRepository(mongoConnection, eventdispatch.NewInMemoryEventDispatcher(mr))
	reportingRepository := mongodbimpl.NewReportingRepository(mongoConnection)
	geometryRepository := mongodbimpl.NewGeometryRepository(mongoConnection)

	mr.Register(commandhandlers.NewCreateOrderCommandHandler(orderRepository).Handle)
	mr.Register(commandhandlers.NewDeleteOrderCommandHandler(orderRepository).Handle)
	mr.Register(commandhandlers.NewUpdateOrderCommandHandler(orderRepository).Handle)
	mr.Register(queryhandlers.NewGetOrderByIdQueryHandler(orderRepository).Handle)
	mr.Subscribe(eventhandlers.NewOrderCreatedEventHandler(geometryRepository, reportingRepository).Handle)
	mr.Subscribe(eventhandlers.NewOrderDeletedEventHandler(reportingRepository).Handle)
	mr.Subscribe(eventhandlers.NewOrderUpdatedEventHandler(geometryRepository, reportingRepository).Handle)
}
