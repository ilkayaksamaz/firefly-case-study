package httpport

import (
	"firefly/pkg/domain/exceptions"
	"firefly/pkg/infrastructure/http_port/builders"
	"firefly/pkg/infrastructure/http_port/interfaces"

	"github.com/gin-gonic/gin"
)

type httpPort struct {
	responseBuilders map[string]interfaces.ResponseBuilder
}

func (h httpPort) Present(c *gin.Context, payload interface{}, err error) {
	builderName := "Ok"
	var managedError *exceptions.ErrorBase
	if e, ok := err.(*exceptions.ErrorBase); ok {
		builderName = e.Type
		managedError = e
	} else if !ok && err != nil {
		builderName = string(exceptions.InternalServerError)
		managedError = exceptions.NewError("9001", exceptions.InternalServerError, err)
	} else if payload == nil {
		builderName = string(exceptions.NoContent)
	}

	if builder, exists := h.responseBuilders[builderName]; exists {
		builder.Build(c, payload, managedError)
	}
}
func NewHttpPort() interfaces.HttpPort {
	return &httpPort{
		responseBuilders: map[string]interfaces.ResponseBuilder{
			"BadRequest":          builders.NewBadRequestResponseBuilder(),
			"InternalServerError": builders.NewInternalServerErrorResponseBuilder(),
			"NoContent":           builders.NewNoContentResponseBuilder(),
			"Ok":                  builders.NewOkResponseBuilder(),
		},
	}
}
