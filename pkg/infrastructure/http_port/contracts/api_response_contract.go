package contracts

type ApiResponseContract struct {
	Result   interface{}        `json:"result"`
	Messages []*MessageContract `json:"messages"`
}
type MessageContract struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func NewMessageContract(code, message string) *MessageContract {
	return &MessageContract{code, message}
}
