package builders

import (
	"firefly/pkg/domain/exceptions"
	"firefly/pkg/infrastructure/http_port/contracts"
	"firefly/pkg/infrastructure/http_port/interfaces"
	"net/http"

	"github.com/gin-gonic/gin"
)

type noContentResponseBuilder struct{}

func (b noContentResponseBuilder) Build(c *gin.Context, payload interface{}, err *exceptions.ErrorBase) {
	c.JSON(http.StatusNoContent, &contracts.ApiResponseContract{})
}

func NewNoContentResponseBuilder() interfaces.ResponseBuilder {
	return &noContentResponseBuilder{}
}
