package builders

import (
	"firefly/pkg/domain/exceptions"
	"firefly/pkg/infrastructure/http_port/contracts"
	"firefly/pkg/infrastructure/http_port/interfaces"
	"net/http"

	"github.com/gin-gonic/gin"
)

type badRequestResponseBuilder struct{}

func (b badRequestResponseBuilder) Build(c *gin.Context, payload interface{}, err *exceptions.ErrorBase) {
	c.JSON(http.StatusBadRequest, &contracts.ApiResponseContract{
		Messages: []*contracts.MessageContract{
			contracts.NewMessageContract(err.Code, err.Message),
		},
	})
}

func NewBadRequestResponseBuilder() interfaces.ResponseBuilder {
	return &badRequestResponseBuilder{}
}
