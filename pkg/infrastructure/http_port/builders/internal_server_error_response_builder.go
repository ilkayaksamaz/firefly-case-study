package builders

import (
	"firefly/pkg/domain/exceptions"
	"firefly/pkg/infrastructure/http_port/contracts"
	"firefly/pkg/infrastructure/http_port/interfaces"
	"net/http"

	"github.com/gin-gonic/gin"
)

type internalServerErrorResponseBuilder struct{}

func (i internalServerErrorResponseBuilder) Build(c *gin.Context, payload interface{}, err *exceptions.ErrorBase) {
	c.JSON(http.StatusInternalServerError, &contracts.ApiResponseContract{
		Messages: []*contracts.MessageContract{
			contracts.NewMessageContract(err.Code, err.Message),
		},
	})
}

func NewInternalServerErrorResponseBuilder() interfaces.ResponseBuilder {
	return &internalServerErrorResponseBuilder{}
}
