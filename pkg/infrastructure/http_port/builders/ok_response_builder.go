package builders

import (
	"firefly/pkg/domain/exceptions"
	"firefly/pkg/infrastructure/http_port/contracts"
	"firefly/pkg/infrastructure/http_port/interfaces"
	"net/http"

	"github.com/gin-gonic/gin"
)

type okResponseBuilder struct{}

func (b okResponseBuilder) Build(c *gin.Context, payload interface{}, err *exceptions.ErrorBase) {
	c.JSON(http.StatusOK, &contracts.ApiResponseContract{
		Result: payload,
	})
}

func NewOkResponseBuilder() interfaces.ResponseBuilder {
	return &okResponseBuilder{}
}
