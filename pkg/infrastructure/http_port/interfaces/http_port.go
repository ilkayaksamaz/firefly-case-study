package interfaces

import "github.com/gin-gonic/gin"

type HttpPort interface {
	Present(c *gin.Context, payload interface{}, err error)
}
