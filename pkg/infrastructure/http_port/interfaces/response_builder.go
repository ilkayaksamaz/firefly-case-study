package interfaces

import (
	"firefly/pkg/domain/exceptions"

	"github.com/gin-gonic/gin"
)

type ResponseBuilder interface {
	Build(c *gin.Context, payload interface{}, err *exceptions.ErrorBase)
}
