package mongodbimpl

import (
	"context"
	"firefly/pkg/domain/aggreates/order"
	"firefly/pkg/domain/aggreates/order/events"
	eventdispatch "firefly/pkg/domain/event_dispatch"
	"firefly/pkg/domain/exceptions"
	"firefly/pkg/domain/repositories"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type OrderRepositoryMongodbImpl struct {
	collection    *mongo.Collection
	eventdispatch eventdispatch.EventDispatcher
}

func NewOrderRepository(client *mongo.Client, eventdispatch eventdispatch.EventDispatcher) repositories.OrderRepository {
	return &OrderRepositoryMongodbImpl{client.Database("orders").Collection("order"), eventdispatch}
}

func (o *OrderRepositoryMongodbImpl) Get(ctx context.Context, orderId string) (*order.Order, error) {
	order := new(order.Order)
	result := o.collection.FindOne(ctx, bson.M{"_id": orderId})
	err := result.Decode(order)

	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, exceptions.ThrowOrderNotFoundError()
		}
		return nil, err
	}
	return order, nil
}

func (o *OrderRepositoryMongodbImpl) Add(ctx context.Context, order *order.Order) (string, error) {
	result, err := o.collection.InsertOne(ctx, order)
	if err != nil {
		return "", err
	}
	err = o.eventdispatch.DispatchEvent(ctx, events.NewOrderCreatedEvent(order.OrderId, order.Price, order.Latitude, order.Longitude, order.PaymentType))
	return result.InsertedID.(string), err
}
func (o *OrderRepositoryMongodbImpl) Update(ctx context.Context, orderId string, order *order.Order) error {
	_, err := o.collection.ReplaceOne(ctx, bson.M{"_id": orderId}, order)
	if err != nil {
		return err
	}
	err = o.eventdispatch.DispatchEvent(ctx, events.NewOrderUpdatedEvent(order.OrderId, order.Price, order.Latitude, order.Longitude, order.PaymentType))
	return err

}
func (o *OrderRepositoryMongodbImpl) Delete(ctx context.Context, orderId string) error {
	_, err := o.collection.DeleteOne(ctx, bson.M{"_id": orderId})
	if err != nil {
		return err
	}
	err = o.eventdispatch.DispatchEvent(ctx, events.NewOrderDeletedEvent(orderId))
	return err
}
