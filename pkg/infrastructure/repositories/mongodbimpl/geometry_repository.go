package mongodbimpl

import (
	"context"
	"firefly/pkg/domain/repositories"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type GeometryRepositoryMongodbImpl struct {
	collection *mongo.Collection
}

type geometry struct {
	Id string `bson:"_id"`
}

func NewGeometryRepository(client *mongo.Client) repositories.GeometryRepository {
	return &GeometryRepositoryMongodbImpl{client.Database("geometries").Collection("geometry_list")}
}

func (g *GeometryRepositoryMongodbImpl) FindGeometryIdByLatAndLon(ctx context.Context, lat, lon float64) (string, error) {
	filter := bson.M{"geometry": bson.M{"$geoIntersects": bson.M{"$geometry": bson.M{"type": "Point", "coordinates": []float64{lat, lon}}}}}
	result := g.collection.FindOne(ctx, filter)
	geometry := new(geometry)

	if err := result.Decode(geometry); err != nil {
		return "", err
	}
	return geometry.Id, nil
}
