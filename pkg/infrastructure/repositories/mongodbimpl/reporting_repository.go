package mongodbimpl

import (
	"context"
	"firefly/pkg/domain/aggreates/reporting"
	"firefly/pkg/domain/repositories"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type ReportingRepository struct {
	collection *mongo.Collection
}

func NewReportingRepository(client *mongo.Client) repositories.ReportingRepository {
	return &ReportingRepository{client.Database("reports").Collection("report")}
}

func (r *ReportingRepository) Add(ctx context.Context, reporting *reporting.Reporting) (string, error) {
	result, err := r.collection.InsertOne(ctx, reporting)
	if err != nil {
		return "", err
	}
	return result.InsertedID.(string), nil
}
func (r *ReportingRepository) UpdateByOrderId(ctx context.Context, orderId string, reporting *reporting.Reporting) error {
	now := time.Now().UTC()
	update := bson.D{{"$set",
		bson.D{
			{"geometry_id", reporting.GeometryId},
			{"order_point", reporting.OrderPoint},
			{"updated_date", now},
		},
	}}

	_, err := r.collection.UpdateOne(ctx, bson.M{"order_id": orderId}, update)
	if err != nil {
		return err
	}
	return nil
}

func (r *ReportingRepository) DeleteByOrderId(ctx context.Context, orderId string) error {
	_, err := r.collection.DeleteOne(ctx, bson.M{"order_id": orderId})
	return err
}

func (r *ReportingRepository) GetOrderCountByGeometry(ctx context.Context, geometryId string) (int64, error) {
	return r.collection.CountDocuments(ctx, bson.M{"geometry_id": geometryId})
}
