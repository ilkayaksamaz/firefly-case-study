package eventdispatch

import (
	"context"
	eventdispatch "firefly/pkg/domain/event_dispatch"

	"github.com/adzeitor/mediatr"
)

type InMemoryEventDispatcher struct {
	mediator mediatr.Mediator
}

func NewInMemoryEventDispatcher(mediator mediatr.Mediator) eventdispatch.EventDispatcher {
	return &InMemoryEventDispatcher{mediator}
}

func (i *InMemoryEventDispatcher) DispatchEvent(ctx context.Context, event interface{}) error {
	return i.mediator.Publish(ctx, event)
}
