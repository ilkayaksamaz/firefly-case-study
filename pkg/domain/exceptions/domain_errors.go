package exceptions

import (
	"errors"
)

const (
	PriceCantBeLowerOrEqualZeroErrorCode                 = "ORD_1001"
	LatitudeOrLongitudeCantBeLowerOrEqualToZeroErrorCode = "ORD_1002"
	OrderIdCantBeEmptyErrorCode                          = "REP_1001"
	GeometryIdCantBeEmptyErrorCode                       = "REP_1002"
	PointCantBeEmptyErrorCode                            = "REP_1003"
)

var (
	PriceCantBeLowerOrEqualZeroError                 = errors.New("Order price can't be lower or equal to zero")
	LatitudeOrLongitudeCantBeLowerOrEqualToZeroError = errors.New("Latitude/longitude can't be lower or equal to zero")
	OrderIdCantBeEmptyError                          = errors.New("OrderId can't be empty")
	GeometryIdCantBeEmptyError                       = errors.New("GeometryId can't be empty")
	PointCantBeEmptyError                            = errors.New("Point can't be empty")
)

func ThrowPriceCantBeLowerOrEqualZeroError() error {
	return NewError(PriceCantBeLowerOrEqualZeroErrorCode, BadRequest, PriceCantBeLowerOrEqualZeroError)
}

func ThrowLatitudeOrLongitudeCantBeLowerOrEqualToZero() error {
	return NewError(LatitudeOrLongitudeCantBeLowerOrEqualToZeroErrorCode, BadRequest, LatitudeOrLongitudeCantBeLowerOrEqualToZeroError)
}

func ThrowOrderIdCantBeEmpty() error {
	return NewError(OrderIdCantBeEmptyErrorCode, BadRequest, OrderIdCantBeEmptyError)
}

func ThrowGeometryIdCantBeEmpty() error {
	return NewError(GeometryIdCantBeEmptyErrorCode, BadRequest, GeometryIdCantBeEmptyError)
}

func ThrowPointCantBeEmpty() error {
	return NewError(PointCantBeEmptyErrorCode, BadRequest, PointCantBeEmptyError)
}
