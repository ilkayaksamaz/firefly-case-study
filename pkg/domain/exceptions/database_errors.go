package exceptions

import (
	"errors"
)

const (
	DocmentNotFoundErrorCode   = "ORD_2001"
	ReportingNotFoundErrorCode = "REP_2001"
)

var (
	DocmentNotFoundError   = errors.New("Order not found")
	ReportingNotFoundError = errors.New("Reporting not found")
)

func ThrowOrderNotFoundError() error {
	return NewError(DocmentNotFoundErrorCode, BadRequest, DocmentNotFoundError)
}

func ThrowReportingNotFoundError() error {
	return NewError(ReportingNotFoundErrorCode, BadRequest, ReportingNotFoundError)
}
