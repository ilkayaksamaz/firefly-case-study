package exceptions

import (
	"fmt"
)

type ErrorType string

const (
	BadRequest          ErrorType = "BadRequest"
	InternalServerError ErrorType = "InternalServerError"
	NoContent           ErrorType = "NoContent"
)

type ErrorBase struct {
	Code    string
	Message string
	Type    string
	error
}

func NewError(code string, errorType ErrorType, err error) *ErrorBase {
	return &ErrorBase{
		Code:    code,
		Message: err.Error(),
		Type:    string(errorType),
		error:   fmt.Errorf("%s %w", code, err),
	}
}

func (e ErrorBase) String() string {
	return fmt.Sprintf("%s %s", e.Code, e.Message)
}
