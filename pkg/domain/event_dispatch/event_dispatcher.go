package eventdispatch

import (
	"context"
)

type EventDispatcher interface {
	DispatchEvent(ctx context.Context, event interface{}) error
}
