package aggreates

import "time"

type AggregateRoot struct {
	CreatedDate *time.Time `bson:"created_date"`
	UpdatedDate *time.Time `bson:"updated_date"`
}
