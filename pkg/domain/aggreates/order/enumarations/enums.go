package enumarations

type PaymentType int

const (
	CASH       PaymentType = 1
	CREDITCARD PaymentType = 2
	CHECK      PaymentType = 3
)
