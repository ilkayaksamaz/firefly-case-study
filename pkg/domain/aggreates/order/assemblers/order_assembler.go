package assemblers

import (
	"firefly/pkg/domain/aggreates/order"
	"firefly/pkg/domain/aggreates/order/contracts"
)

func ToContract(aggregate *order.Order) *contracts.OrderContract {
	return &contracts.OrderContract{
		Id:          aggregate.OrderId,
		Price:       aggregate.Price,
		PaymentType: aggregate.PaymentType,
		Latitude:    aggregate.Latitude,
		Longitude:   aggregate.Latitude,
	}
}

func ToContracts(aggregate []*order.Order) []*contracts.OrderContract {

	result := make([]*contracts.OrderContract, 0)

	for _, a := range aggregate {
		result = append(result, ToContract(a))
	}
	return result
}
