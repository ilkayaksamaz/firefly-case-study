package order

import (
	"firefly/pkg/domain/aggreates/order/commands"
	"firefly/pkg/domain/aggreates/order/enumarations"
	"firefly/pkg/domain/exceptions"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func GetOrder() (*Order, error) {
	price, paymentType, latitude, longitude := float64(10), enumarations.CASH, float64(1.1), float64(1.2)
	command := commands.NewCreateOrderCommand(price, latitude, longitude, paymentType)
	return NewOrder(command)
}
func TestOrder_NewOrder(t *testing.T) {
	//arrange
	price, paymentType, latitude, longitude := float64(10), enumarations.CASH, float64(1.1), float64(1.2)
	command := commands.NewCreateOrderCommand(price, latitude, longitude, paymentType)
	//act
	order, err := NewOrder(command)
	//assert
	assert.Nil(t, err)
	assert.NotNil(t, order)
	assert.NotNil(t, order.OrderId)
}

func TestOrder_NewOrder_ThrowException_WhenPriceIsLowerThanOrEqualZero(t *testing.T) {
	//arrange
	price, paymentType, latitude, longitude := float64(0), enumarations.CASH, float64(1.1), float64(1.2)
	command := commands.NewCreateOrderCommand(price, latitude, longitude, paymentType)
	//act
	order, err := NewOrder(command)
	//assert
	assert.NotNil(t, err)
	assert.Nil(t, order)
	assert.True(t, err.(*exceptions.ErrorBase).Code == exceptions.PriceCantBeLowerOrEqualZeroErrorCode, "wrong error type")
}

func TestOrder_NewOrder_ThrowException_WhenLatititudeOrLongitudeIsLowerThanOrEqualZero(t *testing.T) {
	//arrange
	price, paymentType, latitude, longitude := float64(10), enumarations.CASH, float64(0), float64(1.2)
	command := commands.NewCreateOrderCommand(price, latitude, longitude, paymentType)
	//act
	order, err := NewOrder(command)
	//assert
	assert.NotNil(t, err)
	assert.Nil(t, order)
	assert.True(t, err.(*exceptions.ErrorBase).Code == exceptions.LatitudeOrLongitudeCantBeLowerOrEqualToZeroErrorCode, "wrong error type")
}

func TestOrder_SetLatLong(t *testing.T) {
	//arrange
	order, err := GetOrder()

	lat, lon := float64(1.1), float64(1.2)
	//act
	order.SetLatLong(lat, lon)

	//assert
	assert.Nil(t, err)
	assert.Equal(t, lat, order.Latitude)
	assert.Equal(t, lon, order.Longitude)
}

func TestOrder_SetLatLong_MustThrowExceptionWhenLatitudeLowerOrEqualToZero(t *testing.T) {
	//arrange
	order, err := GetOrder()
	lat, lon := float64(0), float64(1.2)
	//act
	err = order.SetLatLong(lat, lon)

	//assert
	assert.NotNil(t, err, "error couldn't be empty")
	assert.True(t, err.(*exceptions.ErrorBase).Code == exceptions.LatitudeOrLongitudeCantBeLowerOrEqualToZeroErrorCode, "wrong error type")
}

func TestOrder_SetLatLong_MustThrowExceptionWhenLongitudeLowerOrEqualToZero(t *testing.T) {
	//arrange
	order, err := GetOrder()

	lat, lon := float64(1.1), float64(0)
	//act
	err = order.SetLatLong(lat, lon)

	//assert
	assert.NotNil(t, err, "error couldn't be empty")
	assert.True(t, err.(*exceptions.ErrorBase).Code == exceptions.LatitudeOrLongitudeCantBeLowerOrEqualToZeroErrorCode, "wrong error type")

}

func TestOrder_SetPrice(t *testing.T) {
	//arrange
	order, err := GetOrder()

	newPrice := float64(11.5)
	//act
	err = order.SetPrice(newPrice)

	//assert
	assert.Nil(t, err)
	assert.Equal(t, order.Price, newPrice)
}

func TestOrder_SetPrice_MustThrowExceptionWhenPriceLowerOrEqualZero(t *testing.T) {
	//arrange
	order, err := GetOrder()

	newPrice := float64(0)
	//act
	err = order.SetPrice(newPrice)

	//assert
	assert.NotNil(t, err)
	assert.NotEqual(t, order.Price, newPrice)
	assert.True(t, err.(*exceptions.ErrorBase).Code == exceptions.PriceCantBeLowerOrEqualZeroErrorCode, "wrong error type")

}

func TestOrder_ChangeUpdatedDate(t *testing.T) {
	//arrange
	order, err := GetOrder()

	newDate := time.Now().UTC().Add(-time.Hour * 10)
	order.UpdatedDate = &newDate
	//act
	order.SetUpdatedDateToNow()

	//assert
	assert.Nil(t, err)
	assert.NotNil(t, order.UpdatedDate)
	assert.NotEqual(t, order.UpdatedDate, newDate)
}
