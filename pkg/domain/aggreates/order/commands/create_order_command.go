package commands

import "firefly/pkg/domain/aggreates/order/enumarations"

type CreateOrderCommand struct {
	Price       float64                  `json:"price"`
	PaymentType enumarations.PaymentType `json:"payment_type"`
	Latitude    float64                  `json:"latitude"`
	Longitude   float64                  `json:"longitude"`
}

func NewCreateOrderCommand(price, lat, lon float64, paymentType enumarations.PaymentType) *CreateOrderCommand {
	return &CreateOrderCommand{price, paymentType, lat, lon}
}
