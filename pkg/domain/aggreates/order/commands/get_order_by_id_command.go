package commands

type GetOrderByIdCommand struct {
	OrderId string `json:"order_id"`
}

func NewGetOrderByIdCommand(orderId string) *GetOrderByIdCommand {
	return &GetOrderByIdCommand{orderId}
}
