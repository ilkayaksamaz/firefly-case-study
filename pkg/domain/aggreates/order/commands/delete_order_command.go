package commands

type DeleteOrderCommand struct {
	OrderId string `json:"order_id"`
}

func NewDeleteOrderCommand(orderId string) *DeleteOrderCommand {
	return &DeleteOrderCommand{orderId}
}
