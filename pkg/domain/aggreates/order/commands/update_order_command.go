package commands

type UpdateOrderCommand struct {
	OrderId   string  `json:"order_id"`
	Price     float64 `json:"price"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

func NewUpdateOrderCommand(price, lat, lon float64) *UpdateOrderCommand {
	return &UpdateOrderCommand{Price: price, Latitude: lat, Longitude: lon}
}
