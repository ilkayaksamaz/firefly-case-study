package events

import (
	"firefly/pkg/domain/aggreates/order/enumarations"
)

type OrderCreatedEvent struct {
	OrderId     string                   `json:"order_id"`
	Price       float64                  `json:"price"`
	PaymentType enumarations.PaymentType `json:"payment_type"`
	Latitude    float64                  `json:"latitude"`
	Longitude   float64                  `json:"longitude"`
}

func NewOrderCreatedEvent(orderId string, price, lat, lon float64, paymentType enumarations.PaymentType) *OrderCreatedEvent {
	return &OrderCreatedEvent{orderId, price, paymentType, lat, lon}
}
