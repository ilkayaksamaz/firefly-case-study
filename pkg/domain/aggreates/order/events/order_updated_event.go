package events

import "firefly/pkg/domain/aggreates/order/enumarations"

type OrderUpdatedEvent struct {
	OrderId     string                   `json:"order_id"`
	Price       float64                  `json:"price"`
	PaymentType enumarations.PaymentType `json:"payment_type"`
	Latitude    float64                  `json:"latitude"`
	Longitude   float64                  `json:"longitude"`
}

func NewOrderUpdatedEvent(orderId string, price, lat, lon float64, paymentType enumarations.PaymentType) *OrderUpdatedEvent {
	return &OrderUpdatedEvent{orderId, price, paymentType, lat, lon}
}
