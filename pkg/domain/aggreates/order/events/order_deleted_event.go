package events

type OrderDeletedEvent struct {
	OrderId string
}

func NewOrderDeletedEvent(orderId string) *OrderDeletedEvent {
	return &OrderDeletedEvent{OrderId: orderId}
}
