package order

import (
	"firefly/pkg/domain/aggreates"
	"firefly/pkg/domain/aggreates/order/commands"
	"firefly/pkg/domain/aggreates/order/enumarations"
	"firefly/pkg/domain/exceptions"
	"time"

	"github.com/google/uuid"
)

type Order struct {
	*aggreates.AggregateRoot `bson:",inline"`
	OrderId                  string                   `bson:"_id"`
	Price                    float64                  `bson:"price"`
	PaymentType              enumarations.PaymentType `bson:"payment_type"`
	Latitude                 float64                  `bson:"latitude"`
	Longitude                float64                  `bson:"longitude"`
}

func NewOrder(command *commands.CreateOrderCommand) (*Order, error) {
	if command.Price <= 0 {
		return nil, exceptions.ThrowPriceCantBeLowerOrEqualZeroError()
	}

	if command.Latitude <= 0 || command.Longitude <= 0 {
		return nil, exceptions.ThrowLatitudeOrLongitudeCantBeLowerOrEqualToZero()
	}

	id := uuid.New().String()
	createdDate := time.Now().UTC()
	order := &Order{
		OrderId:     id,
		Price:       command.Price,
		PaymentType: command.PaymentType,
		Latitude:    command.Latitude,
		Longitude:   command.Longitude,
		AggregateRoot: &aggreates.AggregateRoot{
			CreatedDate: &createdDate,
			UpdatedDate: nil,
		},
	}
	return order, nil
}

func (o *Order) SetUpdatedDateToNow() {
	now := time.Now().UTC()
	o.UpdatedDate = &now
}

func (o *Order) SetPrice(price float64) error {
	if price <= 0 {
		return exceptions.ThrowPriceCantBeLowerOrEqualZeroError()
	}
	o.Price = price
	o.SetUpdatedDateToNow()
	return nil
}

func (o *Order) SetLatLong(lat, lon float64) error {
	if lat <= 0 || lon <= 0 {
		return exceptions.ThrowLatitudeOrLongitudeCantBeLowerOrEqualToZero()
	}
	o.SetUpdatedDateToNow()
	return nil
}
