package contracts

import "firefly/pkg/domain/aggreates/order/enumarations"

type OrderContract struct {
	Id          string
	Price       float64                  `json:"price"`
	PaymentType enumarations.PaymentType `json:"payment_type"`
	Latitude    float64                  `json:"latitude"`
	Longitude   float64                  `json:"longitude"`
}
