package reporting

import (
	valueobjects "firefly/pkg/domain/aggreates/reporting/value_objects"
	"firefly/pkg/domain/exceptions"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewReporting(t *testing.T) {
	//arrage
	orderId := "order_id"
	geometryId := "geometry_id"
	lat, lon := 1.1, 1.2
	point, pointErr := valueobjects.NewPoint(lat, lon)

	//act
	reporting, err := NewReporting(orderId, geometryId, point)

	//assert
	assert.Nil(t, pointErr)
	assert.Nil(t, err)
	assert.NotNil(t, reporting)
	assert.NotNil(t, point)
	assert.True(t, reporting.OrderPoint.Equal(point))
}

func TestNewReporting_MustThrowExceptionWhenOrderIdEmpty(t *testing.T) {
	//arrage
	orderId := ""
	geometryId := "geometry_id"
	lat, lon := 1.1, 1.2
	point, _ := valueobjects.NewPoint(lat, lon)

	//act
	_, err := NewReporting(orderId, geometryId, point)

	//assert
	assert.NotNil(t, err)
	assert.True(t, err.(*exceptions.ErrorBase).Code == exceptions.OrderIdCantBeEmptyErrorCode, "wrong error type")

}

func TestNewReporting_MustThrowExceptionWhenGeometryIdEmpty(t *testing.T) {
	//arrage
	orderId := "order_id"
	geometryId := ""
	lat, lon := 1.1, 1.2
	point, _ := valueobjects.NewPoint(lat, lon)

	//act
	_, err := NewReporting(orderId, geometryId, point)

	//assert
	assert.NotNil(t, err)
	assert.True(t, err.(*exceptions.ErrorBase).Code == exceptions.GeometryIdCantBeEmptyErrorCode, "wrong error type")
}

func TestNewReporting_MustThrowExceptionWhenPointEmpty(t *testing.T) {
	//arrage
	orderId := "order_id"
	geometryId := "geometry_id"
	//act
	_, err := NewReporting(orderId, geometryId, nil)

	//assert
	assert.NotNil(t, err)
	assert.True(t, err.(*exceptions.ErrorBase).Code == exceptions.PointCantBeEmptyErrorCode, "wrong error type")
}
