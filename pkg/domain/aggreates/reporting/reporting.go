package reporting

import (
	"firefly/pkg/domain/aggreates"
	valueobjects "firefly/pkg/domain/aggreates/reporting/value_objects"
	"firefly/pkg/domain/exceptions"
	"time"

	"github.com/google/uuid"
)

type Reporting struct {
	aggreates.AggregateRoot `bson:",inline"`
	ReportingId             string              `bson:"_id"`
	OrderId                 string              `bson:"order_id"`
	GeometryId              string              `bson:"geometry_id"`
	OrderPoint              *valueobjects.Point `bson:"order_point"`
}

func NewReporting(orderId, geometryId string, point *valueobjects.Point) (*Reporting, error) {
	if orderId == "" {
		return nil, exceptions.ThrowOrderIdCantBeEmpty()
	}
	if geometryId == "" {
		return nil, exceptions.ThrowGeometryIdCantBeEmpty()
	}
	if point == nil {
		return nil, exceptions.ThrowPointCantBeEmpty()
	}
	now := time.Now().UTC()
	uuid := uuid.New().String()
	return &Reporting{ReportingId: uuid, OrderId: orderId, GeometryId: geometryId, OrderPoint: point, AggregateRoot: aggreates.AggregateRoot{CreatedDate: &now}}, nil
}
