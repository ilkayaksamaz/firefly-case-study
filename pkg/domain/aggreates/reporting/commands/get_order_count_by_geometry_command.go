package commands

type GetOrderCountByGeometryCommand struct {
	GeometryId string
}

func NewGetOrderCountByGeometryCommand(geometryId string) *GetOrderCountByGeometryCommand {
	return &GetOrderCountByGeometryCommand{geometryId}
}
