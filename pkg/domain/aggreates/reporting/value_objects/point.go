package valueobjects

import "firefly/pkg/domain/exceptions"

type Point struct {
	Latitude  float64
	Longitude float64
}

func NewPoint(lat, lon float64) (*Point, error) {
	if lat <= 0 || lon <= 0 {
		return nil, exceptions.ThrowLatitudeOrLongitudeCantBeLowerOrEqualToZero()
	}
	return &Point{lat, lon}, nil
}

func (p *Point) Equal(val interface{}) bool {
	if point, ok := val.(*Point); ok {
		return point.Latitude == p.Latitude && point.Longitude == p.Longitude
	}
	return false
}
