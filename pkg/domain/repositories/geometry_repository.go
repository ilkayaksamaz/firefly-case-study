package repositories

import "context"

type GeometryRepository interface {
	FindGeometryIdByLatAndLon(ctx context.Context, lat, lon float64) (string, error)
}
