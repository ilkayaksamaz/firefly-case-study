package repositories

import (
	"context"
	"firefly/pkg/domain/aggreates/reporting"
)

type ReportingRepository interface {
	Add(ctx context.Context, reporting *reporting.Reporting) (string, error)
	UpdateByOrderId(ctx context.Context, orderId string, reporting *reporting.Reporting) error
	DeleteByOrderId(ctx context.Context, orderId string) error
	GetOrderCountByGeometry(ctx context.Context, geometryId string) (int64, error)
}
