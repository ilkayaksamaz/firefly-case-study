package repositories

import (
	"context"
	"firefly/pkg/domain/aggreates/order"
)

type OrderRepository interface {
	Get(ctx context.Context, orderId string) (*order.Order, error)
	Add(ctx context.Context, order *order.Order) (string, error)
	Update(ctx context.Context, orderId string, order *order.Order) error
	Delete(ctx context.Context, orderId string) error
}
