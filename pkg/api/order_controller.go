package api

import (
	"context"
	"firefly/pkg/domain/aggreates/order/commands"
	"firefly/pkg/infrastructure/http_port/interfaces"
	"net/http"

	"github.com/adzeitor/mediatr"
	"github.com/gin-gonic/gin"
)

type OrderController struct {
	mediator mediatr.Mediator
	httpport interfaces.HttpPort
}

func NewOrderController(mediator mediatr.Mediator, httpport interfaces.HttpPort) *OrderController {
	return &OrderController{mediator, httpport}
}

// @Summary Get
// @Description Get order by id
// @Produce json
// @Param orderId path string true "order id"
// @Success 200 {object} contracts.ApiResponseContract
// @Failure 400 {string} contracts.ApiResponseContract
// @Failure 500 {string} contracts.ApiResponseContract
// @Router /order/{orderId} [get]
func (o *OrderController) Get(c *gin.Context) {
	orderId := c.Param("orderId")
	result, err := o.mediator.Send(context.Background(), commands.NewGetOrderByIdCommand(orderId))
	o.httpport.Present(c, &result, err)
}

// @Summary Update
// @Description Update order
// @Produce json
// @Param body body commands.UpdateOrderCommand true "order data"
// @Success 200 {object} contracts.ApiResponseContract
// @Failure 400 {string} contracts.ApiResponseContract
// @Failure 500 {string} contracts.ApiResponseContract
// @Router /order/{orderId} [put]
func (o *OrderController) Update(c *gin.Context) {
	orderId := c.Param("orderId")

	command := new(commands.UpdateOrderCommand)
	if err := c.ShouldBindJSON(&command); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	command.OrderId = orderId
	result, err := o.mediator.Send(context.Background(), command)
	o.httpport.Present(c, &result, err)
}

// @Summary Create
// @Description Create order
// @Produce json
// @Param body body commands.CreateOrderCommand true "order data"
// @Success 200 {object} contracts.ApiResponseContract
// @Failure 400 {string} contracts.ApiResponseContract
// @Failure 500 {string} contracts.ApiResponseContract
// @Router /order/{orderId} [post]
func (o *OrderController) Create(c *gin.Context) {
	command := new(commands.CreateOrderCommand)
	if err := c.ShouldBindJSON(&command); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	_, err := o.mediator.Send(context.Background(), command)
	o.httpport.Present(c, nil, err)
}

// @Summary Delete
// @Description Delete order
// @Produce json
// @Param orderId path string true "order id"
// @Router /order/{orderId} [delete]
func (o *OrderController) Delete(c *gin.Context) {
	orderId := c.Param("orderId")
	result, err := o.mediator.Send(context.Background(), commands.NewDeleteOrderCommand(orderId))
	o.httpport.Present(c, &result, err)
}
