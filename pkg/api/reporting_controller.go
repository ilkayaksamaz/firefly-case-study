package api

import (
	"context"
	"firefly/pkg/domain/aggreates/reporting/commands"
	"firefly/pkg/infrastructure/http_port/interfaces"

	"github.com/adzeitor/mediatr"
	"github.com/gin-gonic/gin"
)

type ReportingController struct {
	mediator mediatr.Mediator
	httpport interfaces.HttpPort
}

func NewReportingController(mediator mediatr.Mediator, httpport interfaces.HttpPort) *ReportingController {
	return &ReportingController{mediator, httpport}
}

// @Summary GetCount
// @Description Get total order count by geometryId
// @Produce json
// @Param geometryId path string true "geometry id"
// @Success 200 {object} contracts.ApiResponseContract
// @Failure 400 {string} contracts.ApiResponseContract
// @Failure 500 {string} contracts.ApiResponseContract
// @Router /reporting/{geometryId}/count [get]
func (o *ReportingController) GetCount(c *gin.Context) {
	orderId := c.Param("geometryId")
	result, err := o.mediator.Send(context.Background(), commands.NewGetOrderCountByGeometryCommand(orderId))
	o.httpport.Present(c, &result, err)
}
