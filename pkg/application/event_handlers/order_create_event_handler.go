package eventhandlers

import (
	"context"
	"firefly/pkg/domain/aggreates/order/events"
	"firefly/pkg/domain/aggreates/reporting"
	valueobjects "firefly/pkg/domain/aggreates/reporting/value_objects"
	"firefly/pkg/domain/repositories"
)

type OrderCreatedEventHandler struct {
	geometryRepository  repositories.GeometryRepository
	reportingRepository repositories.ReportingRepository
}

func NewOrderCreatedEventHandler(geometryRepository repositories.GeometryRepository, reportingRepository repositories.ReportingRepository) *OrderCreatedEventHandler {
	return &OrderCreatedEventHandler{geometryRepository, reportingRepository}
}

func (o *OrderCreatedEventHandler) Handle(event *events.OrderCreatedEvent) {
	ctx := context.Background()
	geometryId, err := o.geometryRepository.FindGeometryIdByLatAndLon(ctx, event.Latitude, event.Longitude)
	if err != nil {
		return
	}
	point, err := valueobjects.NewPoint(event.Latitude, event.Longitude)
	if err != nil {
		return
	}
	reporting, err := reporting.NewReporting(event.OrderId, geometryId, point)
	if err != nil {
		return
	}
	_, err = o.reportingRepository.Add(ctx, reporting)
	if err != nil {
		return
	}
}
