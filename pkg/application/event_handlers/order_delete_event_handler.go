package eventhandlers

import (
	"context"
	"firefly/pkg/domain/aggreates/order/events"
	"firefly/pkg/domain/repositories"
)

type OrderDeletedEventHandler struct {
	reportingRepository repositories.ReportingRepository
}

func NewOrderDeletedEventHandler(reportingRepository repositories.ReportingRepository) *OrderDeletedEventHandler {
	return &OrderDeletedEventHandler{reportingRepository}
}
func (o *OrderDeletedEventHandler) Handle(event *events.OrderDeletedEvent) {
	err := o.reportingRepository.DeleteByOrderId(context.Background(), event.OrderId)
	if err != nil {
		return
	}
}
