package eventhandlers

import (
	"context"
	"firefly/pkg/domain/aggreates/order/events"
	"firefly/pkg/domain/aggreates/reporting"
	valueobjects "firefly/pkg/domain/aggreates/reporting/value_objects"
	"firefly/pkg/domain/repositories"
)

type OrderUpdatedEventHandler struct {
	geometryRepository  repositories.GeometryRepository
	reportingRepository repositories.ReportingRepository
}

func NewOrderUpdatedEventHandler(geometryRepository repositories.GeometryRepository, reportingRepository repositories.ReportingRepository) *OrderUpdatedEventHandler {
	return &OrderUpdatedEventHandler{geometryRepository, reportingRepository}
}
func (o *OrderUpdatedEventHandler) Handle(event *events.OrderUpdatedEvent) {
	ctx := context.Background()
	geometryId, err := o.geometryRepository.FindGeometryIdByLatAndLon(ctx, event.Latitude, event.Longitude)
	if err != nil {
		return
	}
	point, err := valueobjects.NewPoint(event.Latitude, event.Longitude)
	if err != nil {
		return
	}
	reporting, err := reporting.NewReporting(event.OrderId, geometryId, point)
	if err != nil {
		return
	}
	err = o.reportingRepository.UpdateByOrderId(ctx, event.OrderId, reporting)
	if err != nil {
		return
	}
}
