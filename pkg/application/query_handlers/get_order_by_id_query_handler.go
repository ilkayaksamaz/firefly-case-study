package queryhandlers

import (
	"context"
	"firefly/pkg/domain/aggreates/order/assemblers"
	"firefly/pkg/domain/aggreates/order/commands"
	"firefly/pkg/domain/aggreates/order/contracts"
	"firefly/pkg/domain/repositories"
)

type GetOrderByIdCommandHandler struct {
	orderRepository repositories.OrderRepository
}

func NewGetOrderByIdQueryHandler(orderRepository repositories.OrderRepository) *GetOrderByIdCommandHandler {
	return &GetOrderByIdCommandHandler{orderRepository}
}

func (coch *GetOrderByIdCommandHandler) Handle(ctx context.Context, command *commands.GetOrderByIdCommand) (*contracts.OrderContract, error) {
	order, err := coch.orderRepository.Get(ctx, command.OrderId)
	if err != nil {
		return nil, err
	}
	return assemblers.ToContract(order), nil
}
