package commandhandlers

import (
	"context"
	"firefly/pkg/domain/aggreates/order"
	"firefly/pkg/domain/aggreates/order/commands"
	"firefly/pkg/domain/repositories"
)

type CreateOrderCommandHandler struct {
	orderRepository repositories.OrderRepository
}

func NewCreateOrderCommandHandler(orderRepository repositories.OrderRepository) *CreateOrderCommandHandler {
	return &CreateOrderCommandHandler{orderRepository}
}

func (coch *CreateOrderCommandHandler) Handle(ctx context.Context, command *commands.CreateOrderCommand) error {
	order, err := order.NewOrder(command)
	if err != nil {
		return err
	}
	_, err = coch.orderRepository.Add(ctx, order)
	if err != nil {
		return err
	}
	return nil
}
