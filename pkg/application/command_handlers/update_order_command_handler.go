package commandhandlers

import (
	"context"
	"firefly/pkg/domain/aggreates/order/commands"
	"firefly/pkg/domain/repositories"
)

type UpdateOrderCommandHandler struct {
	orderRepository repositories.OrderRepository
}

func NewUpdateOrderCommandHandler(orderRepository repositories.OrderRepository) *UpdateOrderCommandHandler {
	return &UpdateOrderCommandHandler{orderRepository}
}

func (coch *UpdateOrderCommandHandler) Handle(ctx context.Context, command *commands.UpdateOrderCommand) error {
	existingOrder, err := coch.orderRepository.Get(ctx, command.OrderId)
	if err != nil {
		return err
	}
	err = existingOrder.SetLatLong(command.Latitude, command.Longitude)
	if err != nil {
		return err
	}
	err = existingOrder.SetPrice(command.Price)
	if err != nil {
		return err
	}
	err = coch.orderRepository.Update(ctx, existingOrder.OrderId, existingOrder)
	if err != nil {
		return err
	}
	return nil
}
