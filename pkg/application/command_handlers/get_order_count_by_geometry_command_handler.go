package commandhandlers

import (
	"context"
	"firefly/pkg/domain/aggreates/reporting/commands"
	"firefly/pkg/domain/aggreates/reporting/contracts"
	"firefly/pkg/domain/exceptions"
	"firefly/pkg/domain/repositories"
)

type GetOrderCountByGeometryCommandHandler struct {
	reportingRepository repositories.ReportingRepository
}

func NewGetOrderCountByGeometryCommandHandler(reportingRepository repositories.ReportingRepository) *GetOrderCountByGeometryCommandHandler {
	return &GetOrderCountByGeometryCommandHandler{reportingRepository}
}

func (g *GetOrderCountByGeometryCommandHandler) Handle(ctx context.Context, command *commands.GetOrderCountByGeometryCommand) (*contracts.OrderByGeometryContract, error) {
	if command.GeometryId == "" {
		return nil, exceptions.ThrowGeometryIdCantBeEmpty()
	}
	count, err := g.reportingRepository.GetOrderCountByGeometry(ctx, command.GeometryId)
	return &contracts.OrderByGeometryContract{Count: count}, err
}
