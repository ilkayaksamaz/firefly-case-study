package commandhandlers

import (
	"context"
	"firefly/pkg/domain/aggreates/order/commands"
	"firefly/pkg/domain/repositories"
)

type DeleteOrderCommandHandler struct {
	orderRepository repositories.OrderRepository
}

func NewDeleteOrderCommandHandler(orderRepository repositories.OrderRepository) *DeleteOrderCommandHandler {
	return &DeleteOrderCommandHandler{orderRepository}
}

func (coch *DeleteOrderCommandHandler) Handle(ctx context.Context, command *commands.DeleteOrderCommand) error {
	err := coch.orderRepository.Delete(ctx, command.OrderId)
	if err != nil {
		return err
	}
	return nil
}
