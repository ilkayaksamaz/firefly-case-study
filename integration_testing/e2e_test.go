package main

import (
	"bytes"
	"encoding/json"
	"firefly/pkg/api"
	"firefly/pkg/domain/aggreates/order/commands"
	"firefly/pkg/domain/aggreates/order/enumarations"
	httpport "firefly/pkg/infrastructure/http_port"
	"firefly/pkg/infrastructure/repositories/mongodbimpl"
	"net/http"
	"net/http/httptest"
	"testing"

	configreader "firefly/framework/config_reader"
	mongodbpersistence "firefly/framework/persistence/mongodb_persistence"

	commandhandlers "firefly/pkg/application/command_handlers"

	eventdispatch "firefly/pkg/infrastructure/event_dispatch"

	eventhandlers "firefly/pkg/application/event_handlers"

	queryhandlers "firefly/pkg/application/query_handlers"

	"github.com/adzeitor/mediatr"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var mr = mediatr.New()
var httpPort = httpport.NewHttpPort()
var reportingController = api.NewReportingController(mr, httpPort)
var orderController = api.NewOrderController(mr, httpPort)

func init() {
	path := "configs"
	fileType := "yaml"
	name := "config"
	configReader, err := configreader.NewConfigReader(name, path, fileType)
	if err != nil {
		panic(err)
	}
	mongoConnection, err := mongodbpersistence.NewMongoPersistence(configReader)
	if err != nil {
		panic(err)
	}
	orderRepository := mongodbimpl.NewOrderRepository(mongoConnection, eventdispatch.NewInMemoryEventDispatcher(mr))
	reportingRepository := mongodbimpl.NewReportingRepository(mongoConnection)
	geometryRepository := mongodbimpl.NewGeometryRepository(mongoConnection)
	mr.Register(commandhandlers.NewGetOrderCountByGeometryCommandHandler(reportingRepository).Handle)

	mr.Register(commandhandlers.NewCreateOrderCommandHandler(orderRepository).Handle)
	mr.Register(commandhandlers.NewDeleteOrderCommandHandler(orderRepository).Handle)
	mr.Register(commandhandlers.NewUpdateOrderCommandHandler(orderRepository).Handle)
	mr.Register(queryhandlers.NewGetOrderByIdQueryHandler(orderRepository).Handle)
	mr.Subscribe(eventhandlers.NewOrderCreatedEventHandler(geometryRepository, reportingRepository).Handle)
	mr.Subscribe(eventhandlers.NewOrderDeletedEventHandler(reportingRepository).Handle)
	mr.Subscribe(eventhandlers.NewOrderUpdatedEventHandler(geometryRepository, reportingRepository).Handle)
}

func TestCreateOrder(t *testing.T) {
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	by, err := json.Marshal(commands.NewCreateOrderCommand(10, 2, 2, enumarations.CASH))
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	b := bytes.NewBuffer(by)
	c.Request, _ = http.NewRequest("POST", "/api/v1/order", b)
	orderController.Create(c)
	assert.Equal(t, 204, w.Code)
}
func TestGetOrderCount(t *testing.T) {
	w := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(w)
	c.Params = []gin.Param{
		{
			Key:   "geometryId",
			Value: "954122b5-8d74-486c-950b-4757526a1a68",
		},
	}
	reportingController.GetCount(c)
	assert.Equal(t, 200, w.Code)
}
