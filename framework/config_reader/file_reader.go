package configreader

import (
	"time"

	"github.com/spf13/viper"
)

type ConfigReader struct {
	configSource *viper.Viper
}

func (e *ConfigReader) GetStringValueByKey(key string) string {
	return e.configSource.GetString(key)
}

func (e *ConfigReader) GetIntValueByKey(key string) int {
	return e.configSource.GetInt(key)
}

func (e *ConfigReader) GetInt64ValueByKey(key string) int64 {
	return e.configSource.GetInt64(key)

}

func (e *ConfigReader) GetFloatValueByKey(key string) float64 {
	return e.configSource.GetFloat64(key)
}

func (e *ConfigReader) GetBooleanValueByKey(key string) bool {
	return e.configSource.GetBool(key)
}

func (e *ConfigReader) GetTimeValueByKey(key string) time.Time {
	return e.configSource.GetTime(key)
}

func (e *ConfigReader) GetDurationValueByKey(key string) time.Duration {
	return e.configSource.GetDuration(key)
}

func (e *ConfigReader) GetStringArrayValueByKey(key string) []string {
	return e.configSource.GetStringSlice(key)

}

func (e *ConfigReader) GetIntArrayValueByKey(key string) []int {
	return e.configSource.GetIntSlice(key)
}

func (e *ConfigReader) GetValueByKey(key string) interface{} {
	return e.configSource.Get(key)
}

func (e *ConfigReader) SetDefaults(defaults map[string]interface{}) ConfigurationSource {
	for key, value := range defaults {
		e.configSource.SetDefault(key, value)
	}
	return e
}

func (e *ConfigReader) SetDefault(key string, value interface{}) ConfigurationSource {
	e.configSource.SetDefault(key, value)
	return e
}

func NewConfigReader(name, path, fileType string) (ConfigurationSource, error) {
	configurationSource := viper.New()
	configurationSource.SetConfigName(name)
	configurationSource.SetConfigType(fileType)
	configurationSource.AddConfigPath(path)
	if err := configurationSource.ReadInConfig(); err != nil {
		return nil, err
	}
	return &ConfigReader{configurationSource}, nil
}
