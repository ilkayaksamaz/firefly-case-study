package mongodbpersistence

import (
	"context"
	"errors"
	"time"

	configreader "firefly/framework/config_reader"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func NewMongoPersistence(condifReader configreader.ConfigurationSource) (*mongo.Client, error) {
	condifReader.SetDefault("database.timeout", 10)
	timeout := condifReader.GetIntValueByKey("database.timeout")
	connectionString := condifReader.GetStringValueByKey("database.conn")
	if connectionString == "" {
		return nil, errors.New("connection string could not be empty")
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(timeout)*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(connectionString))
	if err != nil {
		return nil, err
	}
	return client, nil
}
