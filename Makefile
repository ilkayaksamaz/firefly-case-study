INTEGRATION_TEST_PATH=./integration_testing
test:
	go test -tags=integration $(INTEGRATION_TEST_PATH) -count=1 -run=$(INTEGRATION_TEST_SUITE_PATH)
all:
	go test ./... -tags=all